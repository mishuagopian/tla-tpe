Automatas, Teoria de Lenguajes y Compiladores
Trabajo practico especial  - 1er cuatr. 2015
Grupo Theodotus

------------------------------------------------------------------------------------------

Instrucciones de uso

1) Ejecutar el comando "make" desde la raiz del directorio principal.
2) Ejecutar el comando "sh compile.sh archivo", donde "archivo" representa el nombre
 del programa en el lenguaje propuesto por el grupo a compilar que posee extension ".theodotus".
3) Ejecutar el comando "./archivo" para correr el programa propuesto.

------------------------------------------------------------------------------------------

Ejemplo:

Sea "factorial.theodotus" el archivo escrito en el lenguaje propuesto por el grupo, los
 comandos a ingresar para poder ejecutarlo son los siguientes:
1) make
2) sh compile.sh factorial
3) ./factorial