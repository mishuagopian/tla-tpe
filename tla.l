%{
	#include "y.tab.h"
%}

%%

"if"					{ return IF; }
"then"					{ return THEN; }
"do"					{ return DO; }
"while"					{ return WHILE; }
"print"					{ return PRINT; }
"getnum"				{ return GETNUM; }
"eq"					{ return EQ; }
"="						{ return SET;}
"main"					{ return MAIN; }
"void"					{ return VOID; }
"neq"					{ return NEQ; }
"return"				{ return RET; }
"object"				{ return OBJ; }
"'"						{ return QUOTE; }
"int"|"char*"			{ yylval.line = strdup(yytext); return TYPE; }
[ \t\n]					;
[a-zA-Z]+				{ yylval.line = strdup(yytext); return var; }
-?[1-9][0-9]*			{ yylval.line = strdup(yytext); return number; }
"0"						{ yylval.line = strdup(yytext); return number; }
"("						{ yylval.line = strdup(&yytext[0]); return OPEN_PARENTHESES; }
")"						{ yylval.line = strdup(&yytext[0]); return CLOSE_PARENTHESES; }
"{"						{ yylval.line = strdup(&yytext[0]); return OPEN_BRACKET; }
"}"						{ yylval.line = strdup(&yytext[0]); return CLOSE_BRACKET; }
";"						{ yylval.line = strdup(&yytext[0]); return SEMICOLON; }
"+"						{ yylval.line = strdup(&yytext[0]); return PLUS; }
"-"						{ yylval.line = strdup(&yytext[0]); return MINUS; }
"*"						{ yylval.line = strdup(&yytext[0]); return TIMES; }
"%"						{ yylval.line = strdup(&yytext[0]); return MODULE; }
","						{ yylval.line = strdup(&yytext[0]); return COMMA; }
"."						{ yylval.line = strdup(&yytext[0]); return DOT; }
"<"						{ yylval.line = strdup(&yytext[0]); return LT; }
">"						{ yylval.line = strdup(&yytext[0]); return GT; }

%%

int yywrap (void){
	return 1;
}
