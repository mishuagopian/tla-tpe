#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <ctype.h>
typedef enum {INITIAL,
	OBJECT,
	OBJECT_NAME,
	READING_TYPE,
	READING_NAME,
	READING_OBJECT_FUNCTION,
	READING_OBJECT_FUNCTION_PARAMETERS,
	READING_FUNCTION,
	NO_OBJECTS
} states;

int main(void){
	int c, flag;
	char name[100];
	char vars[100][100];
	char buffer[100];
	int state = INITIAL;
	int i = 0;
	int j = 0;
	int k = 0;
	int brackets = 0;
	FILE * file = fopen("out.c", "w");
	char * includes = "#include <stdio.h>\n#include \"getnum.h\"\n";
	fputs(includes, file);
	while((c=getchar())!=EOF){
		switch(state){
			case OBJECT:{
				i++;
				if(i == 6){
					state = OBJECT_NAME;
					i = 0;
					j = 0;
				}
				break;
			}
			case INITIAL:{
				if(c == ' ' || c == '\n') {
					break;
				}else if(c == 'o'){
					i++;
					state = OBJECT;
				}else{
					fputc(c, file);
					state = NO_OBJECTS;
				}
				break;
			}
			case OBJECT_NAME:{
				if(c == ' ' && i == 0){
					break;
				}
				if(c == ' '){
					buffer[i] = '\0';
					strcpy(name, buffer);
					state = READING_TYPE;
					i = 0;
				}else{
					buffer[i] = c;
					i++;
				}
				break;
			}
			case READING_TYPE:{
				if(!isalpha(c) && i == 0 && c != '}'){
					break;
				}
				if(c == '}'){
					state = INITIAL;
					break;
				}
				if(isalpha(c)){
					buffer[i] = c;
					i++;
				}else if(c == ' '){
					buffer[i] = c;
					buffer[i+1] = '\0';
					fputs(buffer, file);
					state = READING_NAME;
					i = 0;
				}
				break;
			}
			case READING_NAME:{
				if(isalpha(c)){
					buffer[i] = c;
					i++;
				}else if(c == ' ' || c == ';'){
					buffer[i] = '\0';
					strcpy(vars[j], buffer);
					j++;
					fputs(name, file);
					fputc('_', file);
					fputs(buffer, file);
					fputc(c, file);
					if(c == ';'){
						state = READING_TYPE;					
					}else{
						state = READING_OBJECT_FUNCTION_PARAMETERS;
					}
					i = 0;
				}
				break;	
			}
			case READING_OBJECT_FUNCTION_PARAMETERS:{
				fputc(c, file);
				if(c == ')'){
					state = READING_OBJECT_FUNCTION;
				}
				break;
			}
			case READING_OBJECT_FUNCTION:{
				if(isalpha(c) || c == '_'){
					buffer[i] = c;
					i++;
				}else{
					if(c == '{'){
						brackets++;
					}else if(c == '}'){
						brackets--;
						if(brackets == 0){
							state = READING_TYPE;
						}
					}
					buffer[i] = '\0';
					if(i!=0){
						flag = 0;
						for(k = 0; k < j && !flag; k++){
							if(!strcmp(buffer, vars[k])){
								flag = 1;
								fputs(name, file);
								fputc('_', file);
								fputs(buffer, file);
								fputc(c, file);
							}
						}
						if(!flag){
							fputs(buffer, file);
							fputc(c, file);
						}
						i = 0;
					}else{
						fputc(c, file);
					}
				}
				break;
			}
			case NO_OBJECTS:{
				fputc(c, file);
				break;
			}
		}
	}
	fclose(file);
	return 1;
}