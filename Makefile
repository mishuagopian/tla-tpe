all:
	yacc -d tla.y
	lex tla.l
	gcc -o tla lex.yy.c y.tab.c 
	gcc -o parser parser.c
	
clean:
	rm -f tla
	rm -f parser
	rm -f lex.yy.c
	rm -f y.tab.c
	rm -f y.tab.h
	rm -f out.c
