%{
	int yydebug = 1;
	void yyerror(char *s);
	#include <stdio.h>
	#include <stdlib.h>
	#include <stdarg.h>
	#include <string.h>
	char* concat_str(int argc, ...);
%}

%union {char *line;}
%start S
%token IF THEN
%token DO WHILE
%token PRINT
%token EQ NEQ
%token MAIN
%token GETNUM
%token VOID
%token SEMICOLON COMMA DOT QUOTE
%token OPEN_BRACKET CLOSE_BRACKET
%token OPEN_PARENTHESES CLOSE_PARENTHESES
%token <line> OBJ
%token PLUS MINUS TIMES MODULE SET
%token RET
%token LT GT
%token <line> number
%token <line> var
%token <line> TYPE 
%type <line> A condition block prod sum sub assign declare call mod DECLARES VARS FUNCTIONS FUNCTION PARAMETERS MAIN_FUNCTION OBJECTS OBJECT OBJ_CONTENT

%%

S 					:	OBJECTS FUNCTIONS MAIN_FUNCTION 	{ return printf("%s%s%s\n", $1, $2, $3); }
					|	OBJECTS MAIN_FUNCTION				{ return printf("%s%s\n", $1, $2); }
					|	FUNCTIONS MAIN_FUNCTION				{ return printf("%s%s\n", $1, $2); }
					|	MAIN_FUNCTION						{ return printf("%s\n", $1); }
					;

OBJECTS				:	OBJECTS OBJECT 	 					{ $$ = concat_str(3, $1, "\n", $2); }
					|	OBJECT 								{ $$ = concat_str(2, $1, "\n"); }
					;

OBJECT 				:	OBJ var OPEN_BRACKET OBJ_CONTENT CLOSE_BRACKET	
															{ $$ = concat_str(5, "object ", $2, " {\n", $4, "}\n"); }
					;

OBJ_CONTENT			:	DECLARES FUNCTIONS					{ $$ = concat_str(2, $1, $2); }
					|	FUNCTIONS							{ $$ = concat_str(1, $1); }
					|	DECLARES							{ $$ = concat_str(1, $1); }
					;

FUNCTIONS			:	FUNCTIONS FUNCTION    				{ $$ = concat_str(3, $1, "\n", $2); }
					|	FUNCTION 							{ $$ = concat_str(2, $1, "\n"); }
					;

FUNCTION 			:	TYPE var OPEN_PARENTHESES PARAMETERS CLOSE_PARENTHESES OPEN_BRACKET block CLOSE_BRACKET
															{ $$ = concat_str(8, $1," ", $2, " (", $4, ") {\n",$7,"}\n"); }
					;

PARAMETERS			:	PARAMETERS COMMA TYPE var 			{ $$ = concat_str(5, $1, ", ", $3, " ", $4); }
					|	TYPE var 							{ $$ = concat_str(3, $1, " ", $2); }
					|	VOID 								{ $$ = concat_str(1, "void"); }
					;
					

MAIN_FUNCTION 		:	TYPE MAIN OPEN_PARENTHESES VOID CLOSE_PARENTHESES OPEN_BRACKET block CLOSE_BRACKET																		{ $$ = concat_str(4, $1, " main (void){\n", $7, "}\n"); }
					;

A 					:	IF OPEN_PARENTHESES condition CLOSE_PARENTHESES THEN OPEN_BRACKET block CLOSE_BRACKET		
															{ $$ = concat_str(5,"if(",$3,"){\n",$7,"}\n"); }
					|	DO OPEN_BRACKET block CLOSE_BRACKET WHILE OPEN_PARENTHESES condition CLOSE_PARENTHESES 		
															{ $$ = concat_str(5,"do{\n",$3,"}while(",$7,");\n"); }
					| 	PRINT var SEMICOLON					{ $$ = concat_str(3,"printf(\"%d\\n\",",$2,");\n"); }
					|	PRINT number SEMICOLON				{ $$ = concat_str(3,"printf(\"",$2,"\\n\");\n"); }
					|	PRINT QUOTE VARS QUOTE SEMICOLON	{ $$ = concat_str(3,"printf(\"",$3," \\n\");\n");}
					|	RET number SEMICOLON 				{ $$ = concat_str(3,"return ",$2,";\n");}
					|	RET var SEMICOLON 					{ $$ = concat_str(3,"return ",$2,";\n");}
					;

block				:	A block										{ $$ = concat_str(2,$1,$2); }
					|	A SEMICOLON									{ $$ = concat_str(1,$1); }
					|	assign block								{ $$ = concat_str(2,$1,$2); }
					|	assign SEMICOLON							{ $$ = concat_str(1,$1); }
					|	declare block								{ $$ = concat_str(2,$1,$2); }
					|	declare SEMICOLON							{ $$ = concat_str(1,$1); }
					|	prod block									{ $$ = concat_str(2,$1,$2); }
					|	prod SEMICOLON								{ $$ = concat_str(1,$1);}
					|	sum block									{ $$ = concat_str(2,$1,$2); }
					|	sum SEMICOLON								{ $$ = concat_str(1,$1);}
					|	sub block									{ $$ = concat_str(2,$1,$2); }
					|	sub SEMICOLON								{ $$ = concat_str(1,$1);}
					|	mod block									{ $$ = concat_str(2,$1,$2); }
					|	mod SEMICOLON								{ $$ = concat_str(1,$1);}
					|	call block									{ $$ = concat_str(2,$1,$2); }
					|	call SEMICOLON								{ $$ = concat_str(1,$1);}
					;

condition			:	var EQ var 									{ $$ = concat_str(3, $1, "==", $3); }
					|	number EQ number							{ $$ = concat_str(3, $1, "==", $3); }
					|	var EQ number 								{ $$ = concat_str(3, $1, "==", $3); }
					|	number EQ var 								{ $$ = concat_str(3, $1, "==", $3); }
					|	var NEQ var 								{ $$ = concat_str(3, $1, "!=", $3); }
					|	number NEQ number							{ $$ = concat_str(3, $1, "!=", $3); }
					|	var NEQ number 								{ $$ = concat_str(3, $1, "!=", $3); }
					|	number NEQ var 								{ $$ = concat_str(3, $1, "!=", $3); }
					|	var GT var 									{ $$ = concat_str(3, $1, ">", $3); }
					|	number GT number							{ $$ = concat_str(3, $1, ">", $3); }
					|	var GT number 								{ $$ = concat_str(3, $1, ">", $3); }
					|	number GT var 								{ $$ = concat_str(3, $1, ">", $3); }
					|	var LT var 									{ $$ = concat_str(3, $1, "<", $3); }
					|	number LT number							{ $$ = concat_str(3, $1, "<", $3); }
					|	var LT number 								{ $$ = concat_str(3, $1, "<", $3); }
					|	number LT var 								{ $$ = concat_str(3, $1, "<", $3); }
					;
prod				:	var TIMES var SEMICOLON						{ $$ = concat_str(4, $1, "*", $3, ";"); }
					|	number TIMES number	SEMICOLON				{ $$ = concat_str(4, $1, "*", $3, ";"); }
					|	var TIMES number SEMICOLON					{ $$ = concat_str(4, $1, "*", $3, ";"); }
					|	number TIMES var SEMICOLON					{ $$ = concat_str(4, $1, "*", $3, ";"); }
					;

mod					:	var MODULE var SEMICOLON					{ $$ = concat_str(4, $1, "%", $3, ";"); }
					|	number MODULE number SEMICOLON				{ $$ = concat_str(4, $1, "%", $3, ";"); }
					|	var MODULE number SEMICOLON					{ $$ = concat_str(4, $1, "%", $3, ";"); }
					|	number MODULE var SEMICOLON					{ $$ = concat_str(4, $1, "%", $3, ";"); }
					;

sum					:	var PLUS var SEMICOLON						{ $$ = concat_str(4, $1, "+", $3, ";"); }
					|	number PLUS number SEMICOLON				{ $$ = concat_str(4, $1, "+", $3, ";"); }
					|	var PLUS number SEMICOLON 					{ $$ = concat_str(4, $1, "+", $3, ";"); }
					|	number PLUS var SEMICOLON					{ $$ = concat_str(4, $1, "+", $3, ";"); }
					;

sub					:	var MINUS var SEMICOLON						{ $$ = concat_str(4, $1, "-", $3, ";"); }
					|	number MINUS number	SEMICOLON				{ $$ = concat_str(4, $1, "-", $3, ";"); }
					|	var MINUS number SEMICOLON					{ $$ = concat_str(4, $1, "-", $3, ";"); }
					|	number MINUS var SEMICOLON					{ $$ = concat_str(4, $1, "-", $3, ";"); }
					;

DECLARES			: 	DECLARES declare 							{ $$ = concat_str(2, $1, $2); }
					|	declare 									{ $$ = concat_str(1, $1); }
					;

declare				:	TYPE var SEMICOLON							{ $$ = concat_str(4,$1," ",$2,";\n"); }
					;

assign				:	var SET number SEMICOLON					{ $$ = concat_str(4, $1, "=", $3, ";\n"); }
					|	var SET var SEMICOLON						{ $$ = concat_str(4, $1, "=", $3, ";\n"); }
					|	var SET QUOTE VARS QUOTE SEMICOLON			{ $$ = concat_str(4, $1, "= \"", $4, "\";\n"); }
					|	var SET GETNUM SEMICOLON					{ $$ = concat_str(3, $1, "=", "getint(\"\");\n"); }
					|	var SET prod 								{ $$ = concat_str(4, $1, "=", $3, "\n"); }
					|	var SET sum 								{ $$ = concat_str(4, $1, "=", $3, "\n"); }
					|	var SET sub 								{ $$ = concat_str(4, $1, "=", $3, "\n"); }
					|	var SET mod 								{ $$ = concat_str(4, $1, "=", $3, "\n"); }
					|	var SET call								{ $$ = concat_str(4, $1, "=", $3, "\n"); }
					;

call				:	var DOT var OPEN_PARENTHESES CLOSE_PARENTHESES SEMICOLON
																	{ $$ = concat_str(4, $1, "_", $3, "();\n"); }
					|	var DOT var OPEN_PARENTHESES VARS CLOSE_PARENTHESES	SEMICOLON
																	{ $$ = concat_str(6, $1, "_", $3, "(", $5, ");\n"); }
					|	var DOT var SEMICOLON						{ $$ = concat_str(4, $1, "_", $3, ";"); }
					;

VARS				:	VARS COMMA var 								{ $$ = concat_str(3, $1, ",", $3); }
					|	var											{ $$ = concat_str(1, $1); }
					|	VARS var 									{ $$ = concat_str(3, $1, " ", $2); }
					;

%%

char* 
concat_str(int argc, ...){
	
   char * ans = NULL;
   char ** args = (char **)malloc(argc*sizeof(char *));

   int size = 0, i;

   va_list ap;
   va_start(ap, argc);
   
   for(i = 0; i < argc; i++)
   {
      args[i] = va_arg(ap, char *);
      size += strlen(args[i]);
   }

   ans = (char *)malloc((size+1)*sizeof(char)); // size+1 para el '\0'

   for(i = 0; i < argc; i++)
      sprintf(ans, "%s%s", ans, args[i]);

   va_end(ap);
   return ans;
}

int main(void){
	return yyparse();
}

void yyerror (char *s){
	fprintf(stderr, "%s\n", s);
}
